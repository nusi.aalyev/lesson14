package main

import (
	"fmt"
	"log"
	"net/http"
)

var Port = ":80"

func main() {

	http.HandleFunc("/", ServeFiles)
	fmt.Println("Serving @ : ", "http://127.0.0.1"+Port)
	log.Fatal(http.ListenAndServe(Port, nil))
}

func ServeFiles(w http.ResponseWriter, r *http.Request) {

	switch r.Method {

	case "GET":

		path := r.URL.Path

		if path == "/" {

			path = "./public/view/index.html"
		} else {

			path = "." + path
		}

		http.ServeFile(w, r, path)

	case "POST":

		r.ParseMultipartForm(0)

		name := r.FormValue("name")

		fmt.Fprintf(w, "Hello %s!", name)

	default:
		fmt.Fprintf(w, "Request type other than GET or POSt not supported")

	}

}
