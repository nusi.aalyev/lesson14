provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "example" {
  ami                    = "ami-0d1ddd83282187d18"
  instance_type          = "t2.micro"
  key_name               = "gitlab_key.pub"
  vpc_security_group_ids = [aws_security_group.main.id]
  private_ip             = "172.31.19.115"
  tags = {
    "Name" = "Ubuntu"
  }
}


resource "aws_eip" "ubuntu" {
  instance = aws_instance.example.id
  vpc      = true
}

resource "aws_security_group" "main" {
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}